<form role="form"
      action="{{$note->id ?
        route('admin.notes.update', ['note' => $note->id, 'page' => request()->input('page')]) :
        route('admin.notes.store', ['page' => request()->input('page')])}}"
      enctype="multipart/form-data"
      method="POST">
  @if($note->id)
    <input type="hidden" name="_method" value="PUT">{{--PATCH--}}
  @endif
  @csrf
  <div class="box-body">
    <div class="form-group {{$errors->first('customer_id') ? 'has-error' : ''}}">
     <label>Ügyfél</label>
      <select name="customer_id" class="form-control form-control-sm tags">
        @foreach($customers as $customer)
          <option value="{{$customer->id}}" {{old('customer_id') == $customer->id ? 'selected' : ''}}>
            {{$customer->name}} ({{$customer->email}})
          </option>
        @endforeach
      </select>
      @if($errors->first('customer_id'))
        <p class="help-block has-error text-danger red">{{$errors->first('customer_id')}}</p>
      @endif
    </div>
    <div class="form-group {{$errors->first('name') ? 'has-error' : ''}}">
      <label>Cím</label>
      <input type="text" class="form-control" placeholder="Cím" name="title"
             value="{{old('title', $note->name)}}">
      @if($errors->first('title'))
        <span class="help-block">{{$errors->first('title')}}</span>
      @endif
    </div>
    <div class="form-group {{$errors->first('content') ? 'has-error' : ''}}">
      <label for="exampleInputEmail1">Jegyzet</label>
      <textarea class="form-control ckeditor" name="content"
      >{{old('content', $note->content)}}</textarea>
      @if($errors->first('content'))
        <span class="help-block">{{$errors->first('content')}}</span>
      @endif
    </div>
    <div class="form-group {{$errors->first('attachment') ? 'has-error' : ''}}">
     <label>Kép</label>
      <input type="file" name="attachment" id="exampleInputFile">
      @if($errors->first('attachment'))
        <p class="help-block has-error text-danger red">{{$errors->first('attachment')}}</p>
      @endif
    </div>
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
    <button type="submit" class="btn btn-success">Mentés</button>
  </div>
</form>