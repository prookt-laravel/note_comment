@extends('admin.layout.admin-layout')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Kiadvány létrehozása</h3>
        </div>
        @include('admin.notes.form')
      </div>
    </div>
  </div>
@stop

@section('extra-scripts')
  <script src="{{asset('vendor/japonline/laravel-ckeditor/ckeditor.js')}}"></script>

{{--
ez a default működés
<script>
    CKEDITOR.replace('.ckeditor');
  </script>--}}
@endsection