@extends('admin.layout.admin-layout')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Kiadványok</h3>
          <div class="card-tools">
            <a href="{{route('admin.notes.create')}}" class="btn btn-success btn-sm">Kiadvány létrehozás <i class="fa fa-fw fa-sm fa-plus"></i></a>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <form action="{{route('admin.notes.index')}}" method="GET">
            <table class="table table-hover table-striped text-nowrap">
              <thead>
              <tr>
                <th>{!! orderTableHeader('id', 'Id') !!}</th>
                <th>{!! orderTableHeader('customer.name', 'Ügyfél') !!}</th>
                <th>{!! orderTableHeader('title', 'Cím') !!}</th>
                <th>{!! orderTableHeader('created_at', 'Dátum') !!}</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td><input type="text" class="form-control form-control-sm" name="search[id]"
                           value="{{request()->input('search.id')}}"></td>
                <td><input type="text" class="form-control form-control-sm" name="search[customer][name]"
                           value="{{request()->input('search.customer.name')}}"></td>
                <td><input type="text" class="form-control form-control-sm" name="search[title]"
                           value="{{request()->input('search.title')}}"></td>
                <td><input type="text" class="form-control form-control-sm" name="search[created_at]"
                           value="{{request()->input('search.created_at')}}"></td>
                <td>
                  <div class="btn-group">
                    <input role="button" type="submit" class="btn btn-primary btn-sm" value="Keresés">
                    <a role="button" class="btn btn-default btn-sm" href="{{route('admin.notes.index')}}"
                       title="Keresési feltételek törlése"><i class="fa fa-sync"></i></a>
                  </div>

                </td>
              </tr>
              @foreach($notes as $note)
                <tr>
                  <td>{{$note->id}}</td>
                  <td>{{$note->customer ? $note->customer->name : ''}}</td>
                  <td>{{$note->title}}</td>
                  <td>{{$note->created_at->format('Y-m-d')}}</td>
                  <td>
                    <div class="btn-group">
                    <a href="{{route('admin.notes.edit', ['note' => $note->id, 'page' => request()->input('page')])}}"
                       class="btn btn-default btn-sm">
                       <i class="far fa-sw fa-sm fa-edit"></i> Módosítás
                     </a>
                      <button data-url="{{route('admin.notes.destroy', $note->id)}}"
                              data-token="{{csrf_token()}}"
                              type="button" role="button"
                              class="btn btn-danger btn-sm confirm-delete">
                        Törlés <i class="far fa-fw fa-sm fa-trash-alt"></i>
                      </button>
                    </div>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </form>
          <div class="d-flex justify-content-center">
            {{ $notes->links('vendor.pagination.bootstrap-4') }}
          </div>

        </div>
      </div>
    </div>
  </div>
@stop
