<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{asset('css/admin.css')}}" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>My </b>Admin
  </div>
  <!-- /.login-logo -->
  <div class="card">
    @include('admin.layout.message')
    <div class="card-body login-card-body">
      <p class="login-box-msg">Bejelentkezés</p>
      @if($errors->first('email'))
        <p class="color-danger">{{$errors->first('email')}}</p>
      @endif
      <form action="{{route('admin.login.store')}}" method="POST">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="email" placeholder="Email"  value="{{old('email')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" name="remember" id="remember">
              <label for="remember">
                Maradjak belépve
              </label>
            </div>
          </div>
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Belépés</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script src="{{asset('js/admin.js')}}"></script>
</body>
</html>
