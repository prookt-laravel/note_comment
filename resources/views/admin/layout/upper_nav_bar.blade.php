<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">

    <li class="nav-item dropdown user-menu">
      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <span class="d-none d-md-inline">{{$admin->name}} <i class="fas fa-fw fa-cog"></i></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
        <li class="user-header bg-primary" style="height:auto;">
          <p>
            {{$admin->email}}
          </p>
        </li>

        <li class="user-footer">
          <a href="{{route('admin.login.logout')}}" class="btn btn-secondary btn-sm btn-flat float-right">Kilépés <i
              class="fas fa-fw fa-sm fa-sign-out-alt"></i></a>
        </li>
      </ul>
    </li>
  </ul>

</nav>
