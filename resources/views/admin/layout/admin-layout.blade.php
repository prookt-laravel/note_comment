<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Admin oldal</title>

  <link href="{{asset('css/admin.css')}}" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    @include('admin.layout.upper_nav_bar')
    @include('admin.layout.left_menu_bar')
    <div class="content-wrapper">
      <div class="content">
        @include('admin.layout.message')
        @include('sse::view')
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('js/admin.js')}}"></script>
  @yield('extra-scripts')
</body>
</html>
