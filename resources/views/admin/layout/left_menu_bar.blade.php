<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{route('admin.dashboard')}}" class="brand-link">
    <span class="brand-text font-weight-light">Admin</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="{{route('admin.dashboard')}}" class="nav-link {{\Route::currentRouteName() == 'admin.dashboard' ? ' active' : ''}}">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item has-treeview {{activeAdminMenu(['customers', 'notes']) ? 'menu-open' : ''}}">
          <a href="#" class="nav-link {{activeAdminMenu(['customers', 'notes']) ? 'active' : ''}}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Tartalom
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{route('admin.customers.index')}}" class="nav-link {{activeAdminMenu('customers') ? 'active' : ''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>Ügyfelek</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.notes.index')}}" class="nav-link {{activeAdminMenu('notes') ? 'active' : ''}}">
                <i class="far fa-circle nav-icon"></i>
                <p>Jegyzetek</p>
              </a>
            </li>
          </ul>
        </li>

      {{--  <li class="nav-item">
          <a href="{{route('admin.customers.index')}}" class="nav-link{{\Route::currentRouteName() == 'admin.customers.index' ? ' active' : ''}}">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Ügyfelek
            </p>
          </a>
        </li>--}}
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>