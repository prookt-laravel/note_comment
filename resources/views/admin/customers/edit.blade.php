@extends('admin.layout.admin-layout')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ügyfél módosítása - {{$customer->name}}</h3>
        </div>
        @include('admin.customers.form')
      </div>
    </div>
  </div>
@stop