<form role="form"
      action="{{$customer->id ? route('admin.customers.update', $customer->id) : route('admin.customers.store')}}"
      enctype="multipart/form-data"
      method="POST">
  @if($customer->id)
    <input type="hidden" name="_method" value="PUT">{{--PATCH--}}
  @endif
  @csrf
  <div class="box-body">
    <div class="form-group {{$errors->first('name') ? 'has-error' : ''}}">
      <label>Név</label>
      <input type="text" class="form-control"  placeholder="Név" name="name"
             value="{{old('name', $customer->name)}}">
      @if($errors->first('name'))
        <span class="help-block">{{$errors->first('name')}}</span>
      @endif
    </div>
    <div class="form-group {{$errors->first('email') ? 'has-error' : ''}}">
      <label for="exampleInputEmail1">Email</label>
      <input type="text" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email"
             value="{{old('email', $customer->email)}}">
      @if($errors->first('email'))
        <span class="help-block">{{$errors->first('email')}}</span>
      @endif
    </div>
    <div class="form-group {{$errors->first('password') ? 'has-error' : ''}}">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
      @if($errors->first('password'))
        <span class="help-block">{{$errors->first('password')}}</span>
      @endif
    </div>
    <div class="form-group {{$errors->first('password_confirmation') ? 'has-error' : ''}}">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="exampleInputPassword1" name="password_confirmation" placeholder="Password">
      @if($errors->first('password_confirmation'))
        <span class="help-block">{{$errors->first('password_confirmation')}}</span>
      @endif
    </div>
    <div class="form-group {{$errors->first('attachment') ? 'has-error' : ''}}">
      <label for="exampleInputFile">Profilkép</label>
      <input type="file" name="attachment" id="exampleInputFile">
      @if($errors->first('attachment'))
        <p class="help-block has-error text-danger red">{{$errors->first('attachment')}}</p>
      @endif
    </div>
    {{-- <div class="checkbox">
       <label>
         <input type="chveckbox"> Check me out
       </label>
     </div>--}}
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
    <button type="submit" class="btn btn-success">Mentés</button>
  </div>
</form>