@extends('admin.layout.admin-layout')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Ügyfelek</h3>
          <div class="card-tools">
            <a href="{{route('admin.customers.create')}}" class="btn btn-primary">Ügyfél létrehozás</a>

            <form action="{{route('admin.customers.sendTestMailToAll')}}" method="POST">
              @csrf
              <button type="submit" class="btn btn-sm btn-warning">Értesítési mail küldése</button>
            </form>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
            <tr>
              <th>{!! orderTableHeader('id', 'Id') !!}</th>
              <th>{!! orderTableHeader('name', 'Név') !!}</th>
              <th>{!! orderTableHeader('email', 'Email') !!}</th>
              <th>Jegyzetek</th>
              <th></th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($customers as $customer)
              <tr>
                <td>{{$customer->id}}</td>
                <td>{{$customer->name}}</td>
                <td>{{$customer->email}}</td>
                <td>
                  <a href="{{route('admin.notes.index', ['search' => ['customer' => ['name' => $customer->name]]])}}"
                     role="button" target="_blank" class="btn btn-sm btn-secondary">
                    Jegyzetek ({{$customer->notes()->count()}})
                  </a>
                </td>
                <td>
                  <form action="{{route('admin.customers.sendTestMail', $customer->id)}}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-warning">Levél küldése</button>
                  </form>
                </td>

                <td>
                  <a href="{{route('admin.customers.edit', $customer->id)}}" class="btn btn-default btn-sm">Módosítás <i
                      class="fa fa-sw fa-sm fa-edit"></i></a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop
