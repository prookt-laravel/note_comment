@extends('frontend.layout.application')

@section('content')

  <h4>Belépés</h4>
  <a href="{{route('social.login', ['provider' => 'facebook'])}}">Belépés facebookkal</a>
  <form action="{{route('login.store')}}" method="POST">
    @csrf
    Email:
    <input type="text" name="email" value="{{old('email')}}">
    <br>
    Password:
    <input type="password" name="password">
    <br><br>
    <button type="submit">Belépés</button>
  </form>

@stop
