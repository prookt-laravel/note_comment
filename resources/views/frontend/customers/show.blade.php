@extends('frontend.layout.application')

@section('content')
  <h1>{{$customer->name}}</h1>
    <img src="{{$customer->attachment->publicUrl()}}">
  {{--ÁTADJUK A $customer-t, mint $object--}}
  @include('frontend.partials._comment_section', ['object' => $customer])
@stop
