<hr>
<h3>Hozzászólások:</h3>
@if(authCustomer())
  <form action="{{route('comments.store', ['type' => get_class($object), 'id' => $object->id])}}" method="POST">
    @csrf
    Komment:
    <textarea name="content"></textarea>
    <button type="submit">Küldés</button>
  </form>
@else
  <p>Hozzászólás írásáshoz jelentkezzen be</p>
@endif
<hr>
@foreach($object->comments()->orderBy('created_at', 'desc')->get() as $comment)
  {{$comment->customer->name}} - {{$comment->created_at->format('Y-m-d H:i:s')}}
  <p>
    {{$comment->content}}
  </p>
  <hr>
@endforeach