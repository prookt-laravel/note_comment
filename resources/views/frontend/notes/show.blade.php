@extends('frontend.layout.application')

@section('content')
  <h2>{{$note->title}}</h2>
  <h4>Író: {{$note->customer->name}}</h4>
  <p>Cimkék:
    @foreach($note->tags as $tag)
      <a href="{{route('notes.index', ['search' => ['tag_id' => $tag->id]])}}">{{$tag->name}}</a>
  @endforeach
  <p>
    {{$note->content}}
  </p>

  @include('frontend.partials._comment_section', ['object' => $note])
@stop
