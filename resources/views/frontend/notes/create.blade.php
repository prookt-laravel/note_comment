@extends('frontend.layout.application')

@section('content')

  <h4>Jegyzet</h4>
    <h3>{{session('message')}}</h3>

    <form action="{{route('notes.store')}}" method="POST">
      @csrf
      Jegyzet:
      <textarea name="content">{{old('content')}}</textarea>
      <br><br>
      Publikálás dátuma
      <input type="text" name="public_at" value="{{old('public_at')}}">

      Cimkék:
      {{--több válasz esetén tömbként küldjük tovább--}}
      <select name="tags[]" multiple>
        @foreach($tags as $tag)
          <option value="{{$tag->id}}">{{$tag->name}}</option>
        @endforeach
      </select>

      <button type="submit">Mentés</button>
    </form>

@stop
