$(document).ready(function () {

  $('.tags').select2({
    locale: 'hu',
  });


  $(document).on('click', '.confirm-delete', function (e) {
    e.preventDefault();

    var token = $(this).data('token');
    var url = $(this).data('url');
    var domToDel = $(this).closest('tr');

    Swal.fire({
      title: 'Biztosan törölni akarja?',
      icon: 'error',
      showCancelButton: true,
      cancelButtonText: 'Mégse',
      confirmButtonText: 'Törlés',
    }).then((result) => {
      if (result.isConfirmed) {
        sendDeleteRequest(url, token, domToDel)
      }
    });
  });

  function sendDeleteRequest(url, token, remove = null) {
    $.ajax({
      url: url,
      type: 'DELETE',
      data: {'_token': token},
      dataType: 'json',
      success: function (data) {
        remove.remove();
      }
    });
  }
});
