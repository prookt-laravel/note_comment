<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminUserSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    User::where('email', 'admin')->delete();

    $admin = new User;
    $admin->name = 'admin';
    $admin->email = 'admin';
    $admin->password  = \Hash::make('1234');
    $admin->save();
  }
}
