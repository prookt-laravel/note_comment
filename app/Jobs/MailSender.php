<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\TestMail;

class MailSender implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $customers;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($customers)
  {
    $this->customers = $customers;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $customers = $this->customers;
    foreach ($customers as $customer) {
     /* for ($i = 0; $i <= 10; $i++) {*/
        try {
          $mail = new TestMail($customer);

          \Mail::to($customer->email)
            ->send($mail);

          \Log::info('Teszt levél kiküldve a következő címre: ' . $customer->email);
        } catch (\Exception $e) {
          \Log::error($e->getMessage());
        }
     /* }*/
    }

    \SSE::notify('Dunno, some message', $type = 'info', $event = 'message');
    }
}
