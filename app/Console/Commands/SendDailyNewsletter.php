<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use App\Jobs\MailSender;


class SendDailyNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:daily-newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends the daily newsletter.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->line($this->description);
      $customers = Customer::all();
      dispatch(new MailSender($customers));

      $this->info('Newsletter sent');
    }
}
