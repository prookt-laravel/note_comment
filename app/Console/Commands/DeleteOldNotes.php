<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use App\Jobs\MailSender;


class SendDailyNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:delete-old-notes {date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes notes older than the given date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->line($this->description);

      $limitDate = \Carbon::parse($this->argument('date'))->format('Y-m-d');

      \App\Models\Note::whereDate('updated_at', '<', $limitDate)->delete();

    }
}
