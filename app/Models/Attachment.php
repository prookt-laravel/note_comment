<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Attachment extends Model
{

  public function attachable()
  {
    return $this->morphTo();
  }


  public function addFile($file)
  {

    if (isset($file) && $file && is_file($file)) {
      $oldName = $file->getClientOriginalName();
      $this->original_filename = $oldName;

      $path = \Storage::disk('dev')->put('/', $file, ['name' => $oldName]);
      $this->path = $path;

      //\Storage::disk('dev')->download($path);

      $this->filename = $file->getClientOriginalName();
    }
  }


  public function publicUrl()
  {
    return \Storage::disk('dev')->url($this->path);
  }
}
