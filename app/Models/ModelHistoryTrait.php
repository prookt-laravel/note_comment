<?php

namespace App\Models;

use App\Observers\ModelHistoryObserver;

trait ModelHistoryTrait {

  protected static function boot()
  {
    parent::boot();

    self::observe(ModelHistoryObserver::class);
  }

  public function getLatestVersion()
  {
    return $this->history()->orderBy('created_at', 'desc')->first();
  }


  public function history()
  {
    return  $this->morphMany(ModelHistory::class, 'historable');
  }


}