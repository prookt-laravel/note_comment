<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Comment extends Model
{

  public function customer()
  {
    return $this->belongsTo(Customer::class);
  }

  public function commentable()
  {
    return $this->morphTo();
  }


/*  public function note()
  {
    return $this->belongsTo(Note::class);
  }*/
}
