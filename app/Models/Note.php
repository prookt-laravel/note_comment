<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Note extends SearchModel
{
  use ModelHistoryTrait;

  public function hasTag($tagId)
  {
    return $this->tags()->find($tagId);
  }

  public function customer()
  { //1-többhöz kapcsolat, 'customer_id'  idegen kulcsal.
    //  return $this->belongsTo(Customer::class/*, 'customer_id'*/);/*Default elnevezésnél nem kell megadni*/
    return $this->belongsTo(Customer::class);
  }

  public function tags()
  {
    return $this->belongsToMany(Tag::class)->withTimestamps();
  }

  public function comments()
  {
    return $this->morphMany(Comment::class, 'commentable');
  }

  public function setAttributes($data)
  {
    $this->content = isset($data['content']) ? $data['content'] : '';
    $this->public_at =  isset($data['public_at']) ? $data['public_at'] : null;
    $this->title =  isset($data['title']) ? $data['title'] : null;
  }

  public function scopeOnFrontend($query)
  {
    $query->whereNotNull('public_at')->whereDate('public_at', '<=', now());
  }

}
