<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelHistory extends Model
{
  protected $table = 'model_history';

  //eloquent tömbként kezeli
  protected $casts = [
    'change' => 'array'
  ];


  public function historable()
  {
    return $this->morphTo();
  }
}