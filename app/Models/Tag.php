<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Tag extends Model
{

  public function notes()
  { //több a többhöz kapcsolat                //ha kellenek a kapcsolótáblába dátumok is
    return $this->belongsToMany(Note::class)->withTimestamps();
    //default tábla elnevezés: note_tag, amit létre is kell hozni.

  }
}
