<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use App\Models\ModelHistoryTrait;
use Laravel\Passport\HasApiTokens;

class Customer extends Authenticatable
{

  use ModelHistoryTrait, HasApiTokens;

  //public $table = 'customers';
  protected $casts = [
    'social' => 'array'
  ];

  public function lastUpdated()
  {
    return $this->updated_at->format('Y-m-d');
  }

  public function notes()
  {
    //1 a többhöz kapcsolat
    return $this->hasMany(Note::class);
  }

  public function comments()
  {
    return $this->morphMany(Comment::class, 'commentable');
  }

  public function attachment()
  {
    return $this->morphOne(Attachment::class, 'attachable');
  }

  public function writtenComments()
  {
    return $this->hasMany(Comment::Class);
  }

  public function setAttributes($data)
  {
    $this->name = $data['name'];
    $this->email = $data['email'];

    if (isset($data['password']) && $data['password']) {
      $this->password = \Hash::make($data['password']);
    }
  }

  public function scopeSearch($query, $data)
  {
    if (isset($data['name']) && $data['name']) {   //$search['name']  = Horvath;
      $query->where('name', 'LIKE', '%' . $data['name'] . '%');
    }

    if (isset($data['email']) && $data['email']) {   //$search['name']  = Horvath;
      $query->where('email', 'LIKE', '%' . $data['email'] . '%');
    }

    if (isset($data['orderby']) && $data['orderby']) {
      $query->orderBy($data['orderby'], $data['order_dir']);
    }
  }

  public function scopeFreshRegister($query)
  {
    //a héten regisztráltak
    $date = Carbon::now()->subWeek()->format('Y-m-d');
    $query->where('created_at', '>', $date);
  }
}
