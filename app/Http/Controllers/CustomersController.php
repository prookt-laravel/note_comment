<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomersController extends Controller
{
  public function show($customerId)
  {
    $customer = Customer::find($customerId);

    return view('frontend.customers.show')->with('customer', $customer);
  }

  public function index(Request $request)
  {
    $search = $request->input('search'); //array
    $search['orderby'] = $request->input('orderby');
    $search['order_dir'] = $request->input('order_dir');

    $customers = Customer::search($search)->get();

    return view('frontend.customers.index')
            ->with('customers', $customers);
  }


}
