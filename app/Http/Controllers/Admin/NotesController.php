<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Note;
use App\Models\Customer;
use App\Models\Attachment;

class NotesController extends Controller
{

  public function index(Request $request)
  {
    $search = $request->input('search'); //array
    $search['sort_by'] = $request->input('orderby');
    $search['sorting_direction'] = $request->input('order_dir');
  //  $notes = Note::search($search)->get();
    $customers = Customer::orderBy('name')->get();

    $notes = Note::search($search)->orderBy('created_at', 'desc')->paginate(5);

    return view('admin.notes.index')
      ->with('notes', $notes)
      ->with('customers', $customers);
  }

  public function create()
  {
    $note = new Note;
    $customers = Customer::orderBy('name')->get();

    return view('admin.notes.create')
        ->with('note', $note)
      ->with('customers', $customers);
  }

  public function store(Request $request)
  {
    $this->validate($request, [   //a customer táblja Idjai közül
      'customer_id' => 'required|exists:customers,id'
    ]);
    $note = new Note;
    $note->setAttributes($request->all());
    $note->customer_id = $request->input('customer_id');
    try {
      $note->save();
      session()->flash('success', 'Kiadvány létrehozva');
    } catch(\Exception $e) {
      session()->flash('error', $e->getMessage());
    }

    return redirect()->route('admin.notes.index', ['page' => $request->input('page')]);
  }

  public function edit($noteId)
  {
    $note = Note::findOrFail($noteId);
    $customers = Customer::orderBy('name')->get();

    return view('admin.notes.edit')
      ->with('note', $note)
      ->with('customers', $customers);
  }

  public function update(Request $request, $noteId)
  {
    $this->validate($request, [
      'customer_id' => 'required|exists:customers,id'
    ]);

    $note = Note::findOrFail($noteId);
    $note->setAttributes($request->all());
    $note->customer_id = $request->input('customer_id');
    $note->save();
    if($file = $request->file('attachment')) {
      $attachment = new Attachment;
      $attachment->addFile($file);
      $attachment->attachable()->associate($note);
      $attachment->save();
    }

    session()->flash('success', 'Kiadvány módosítva');

    return redirect()->route('admin.notes.index', ['page' => $request->input('page')]);
  }

  public function destroy($noteId)
  {
    $note = Note::findOrFail($noteId);
    $note->delete();

    return response()->json(['message' => 'Az ügyfél törölve']);
  }

}
