<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Note;

class DashboardController extends Controller
{

  public function index()
  {
    $newCustomerCount = Customer::whereDate('created_at', '>=', now()->subWeek()->format('Y-m-d'))->count();
    $noteCount = Note::count();

    return view('admin.dashboard.index')
      ->with(compact('newCustomerCount', 'noteCount'));
  }

}
