<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Attachment;
use App\Mail\TestMail;
use App\Jobs\MailSender;

class CustomersController extends Controller
{

  public function index(Request $request)
  {
    $customer = Customer::find(1);

    $search = $request->input('search'); //array
    $search['orderby'] = $request->input('orderby');
    $search['order_dir'] = $request->input('order_dir');
    $customers = Customer::search($search)->get();

    return view('admin.customers.index')
      ->with('customers', $customers);
  }

  public function create()
  {
    $customer = new Customer;

    return view('admin.customers.create')
      ->with('customer', $customer);
  }

  public function store(Request $request)
  {
    $this->validate($request, [

      'name' => 'sajt',
      'email' => 'required|email|unique:customers,email',
      'password' => 'required|confirmed'
    ]);

    $customer = new Customer;
    $customer->setAttributes($request->all());

    try {
      $customer->save();
      session()->flash('success', 'Ügyfél létrehozva');
    } catch (\Exception $e) {
      session()->flash('error', $e->getMessage());
    }
    /* if( $customer->save()) {
       session()->flash('success', 'Ügyfél létrehozva');
     } else {
       session()->flash('error', 'Sajnos a mentés sikertelen');
     }*/


    return redirect()->route('admin.customers.index');
  }

  public function edit($customerId)
  {

    $customer = Customer::findOrFail($customerId);

    return view('admin.customers.edit')->with('customer', $customer);
  }

  public function update(Request $request, $customerId)
  {
    \Validator::extend('sajt', function($attribute, $value, $parameters) {
      return $value == 'sajt';
    });

    $this->validate($request, [
      'name' => 'sajt',
      'name' => 'required',
      'email' => 'required|email|unique:customers,email,' . $customerId,  //unique kivéve a megadott IDra
      'password' => 'confirmed',   //nem kötelező, de ha meg van adva, a 2jelszónak egyeznie kell
      'attachment' => 'file|max:4000|mimes:png,jpg'
    ]);

    $customer = Customer::findOrFail($customerId);
    $customer->setAttributes($request->all());
    $customer->save();
    if ($file = $request->file('attachment')) {
      $attachment = new Attachment;
      $attachment->addFile($file);
      $attachment->attachable()->associate($customer);
      $attachment->save();
    }


    session()->flash('success', 'Ügyfél módosítva');

    return redirect()->route('admin.customers.index');
  }

  public function destroy($customerId)
  {
    $customer = Customer::findOrFail($customerId);
    $customer->delete();

    return response()->json(['message' => 'Az ügyfél törölve']);
  }


  public function sendTestMail($customerId)
  {
    $customer = Customer::findOrFail($customerId);
    $mail = new TestMail($customer);

    /* \Mail::send('mails.test-mail', ['customer' => $customer], function ($message) use ($customer) {
       $message->to($customer->email)
               ->from('')
               ->subject('Test Email');
     });*/
    try {
      \Mail::to($customer->email)
        ->send($mail);

      \Log::info('Teszt levél kiküldve a következő címre: ' . $customer->email);
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
    }
    session()->flash('success', 'Teszt levél kiküldve a következő címre: ' . $customer->email);

    return redirect()->back();
  }


  public function sendTestMailToAll(Request $request)
  {
    $customers = Customer::all();

    dispatch(new MailSender($customers));

    session()->flash('success', 'Teszt levelek kiküldve');
    return redirect()->back();

  }
}
