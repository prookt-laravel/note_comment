<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use App\Models\Customer;
use Carbon\Carbon;
use App\Models\Tag;

class StatsController extends Controller
{

  //Legutóbb kommentelő  3 ügyfél
  //A legtöbbet kommentelő
  //Az az ügyfél, aki a legtöbb kommentet kapta
  //Legtöbbet kommentelt jegyzet
  //legtöbbet commentelt tag
  public function index()
  {
    //Legutóbb kommentelő  3 ügyfél
    $lastCommentsArray = Comment::orderBy('created_at', 'desc')->pluck('customer_id');
    $lastCommentsArray = array_unique($lastCommentsArray);
    $firstThree = array_slice($lastCommentsArray, 0, 3);
    $last3Commenters = Customer::whereIn('id', $firstThree)->get();


     //A legtöbbet kommentelő
     //1
    $customers = Customer::has('comments')->get();
    $mostCommenter = $customers->sortBy(function($customer) {
      $customer->comments->count();
    })->first();

    //2
    $mostCommenter = Comment::selectRaw('customer_id, count(*) AS sum')
      ->groupBy('customer_id')
      ->orderBy('sum', 'desc')
      ->first();

    //3
            //comments_count-ként lesz elérhető
    $mostCommenter = Customer::withCount('comments')->orderBy('comments_count', 'desc')->first();


    //Az az ügyfél, aki a legtöbb kommentet kapta
      //1
    $customerArray = [];
    foreach(Customer::has('notes')->get() as $customerWithNote) {
      $customerCommentCount = 0;
      foreach($customerWithNote->notes as $customerNote) {
        $customerCommentCount += $customerNote->comments()->count();
      }
      $customerArray[$customerWithNote->id] = $customerCommentCount;
    }
          //sorrendezés
    $orderedArray = asort($customerArray);
            //a sorbarendezett tömb utolsó eleme.
    $mostCommentedCustomerId = array_pop($orderedArray);
    $mostCommentedCustomer = Customer::find($mostCommentedCustomerId);

    //Legtöbbet kommentelt jegyzet
    $mostCommentedNote  = Note::withCount('comments')->orderBy('comments_count', 'desc')->first();

    //legtöbbet commentelt tag
    $tagArray = [];
    foreach(Tag::has('notes')->get() as $tagWithNote) {
      $tagCommentCount = 0;
      foreach($tagWithNote->notes as $tagNote) {
        $tagCommentCount += $tagNote->comments()->count();
      }
      $tagArray[$tagWithNote->id] = $tagCommentCount;
    }
    $orderedArray = asort($tagArray);
    $mostCommentedTagId = array_pop($orderedArray);
    $mostCommentedTag = Tag::find($mostCommentedTagId);

    return view('frontend.stats');
  }
}