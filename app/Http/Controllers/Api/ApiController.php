<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

  public function login(Request $request)
  {
    $this->validate($request, [
      'email' => 'required|email',
      'password' => 'required'
    ]);

    $credentials = $request->only('email', 'password');

    auth()->guard('customer')->attempt($credentials);

    $customer = \Auth::guard('customer')->user(); //a belépett customer
    $token = $customer->createToken('Laravel Password Grant Client')->accessToken;

    $response = ['message' => 'yuhuuuu', 'token' => $token];

    return response()->json($response);
  }



  public function getData()
  {
    return response()->json('Ez már a secret data');
  }
}