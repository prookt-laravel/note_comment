<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use App\Models\Customer;
use Carbon\Carbon;
use App\Models\Tag;
use App\Models\Comment;

class CommentsController extends Controller
{

    public function store(Request $request, $type, $id)
    {
      $this->validate($request, [
        'content' => 'required|max:2000'
      ]);
      $model = $type::findOrFail($id);

      $comment = new Comment;
      $comment->customer()->associate(authCustomer());
      $comment->content = $request->input('content');
      $comment->commentable()->associate($model);
      $comment->save();

      return redirect()->back();
    }
}
