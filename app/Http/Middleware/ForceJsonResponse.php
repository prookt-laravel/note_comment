<?php

namespace App\Http\Middleware;


use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Note;


class ForceJsonResponse
{
  public function handle($request, Closure $next)
  {
    $request->headers->set('Accept', 'application/json');

    return $next($request);
  }
}