<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Note;

class OwnNote
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    $noteId = $request->route('noteId');
    if(authCustomer()->notes()->find($noteId)) {
      return $next($request);
    }

    abort(405, 'Permission denied');
  }
}
