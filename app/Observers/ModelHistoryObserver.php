<?php

namespace App\Observers;

use App\Models\ModelHistory;

class ModelHistoryObserver
{

  public function saved($model)
  {
    $this->makeHistory($model);
  }

  public function makeHistory($model)
  {
    $changesArr = [];

    if ($model->isDirty()) {
      $originalAttributes = $model->getOriginal();
      foreach ($model->getDirty() as $attr => $value) {
        $changesArr[$attr] = [
          'old' => isset($originalAttributes[$attr]) ? $originalAttributes[$attr] : null,
          'new' => $value,
        ];
      }

      // $latestVersion = $model->getLatestVersion();
      $history = new ModelHistory;
      $history->historable()->associate($model);
      $history->change = $changesArr;
      $history->version = $model->history()->count() + 1;
      $history->save();
    }
  }


}