<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    \View::composer('admin.*', function ($view) {
      $view->with('admin', auth()->guard('admin')->user());
    });
  }
}
